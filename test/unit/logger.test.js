const logger = require('../../infrastructure/logger')
const PinoLogger = require('../../infrastructure/PinoLogger')

describe('Logger', () => {

  beforeEach(() => {
    jest.resetAllMocks();
  });
  
  test('log errors', () => {
    const spyError = jest.spyOn(PinoLogger, "error")

    logger.error("Any message")

    expect(spyError).toBeCalled()
    expect(spyError).toBeCalledWith("Any message")
  })

  test('log error data', () => {
    const spyError = jest.spyOn(PinoLogger, "error")
    const anyDataObject = { type: "anyType" }
    
    logger.error("Any message", anyDataObject)

    expect(spyError).toBeCalled()
    expect(spyError).toBeCalledWith("Any message", anyDataObject)
  })

})
