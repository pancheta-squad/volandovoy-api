const Healthz = require('../../actions/healthz/Healthz')
const request = require('supertest')
const app = require('../../index')
const SUCCESS = 200
const BAD_REQUEST = 400

describe('The API', () => {

  test('healthz endpoint ensures that is available', async () => {
    const response = await request(app).get('/api/v1/healthz')

    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('Ok')
  })
  
  test('healthz throws an error when fails', async () => {
    const testMessage = 'Test-message: API not respond'
    jest.spyOn(Healthz, 'invoke').mockImplementation(() => {
      return new Promise((resolve, reject) => {
        const error = new Error(testMessage)
        error.statusCode = 400
        reject(error)
      })
    })
    
    const response = await request(app).get('/api/v1/healthz')
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.message).toBe(testMessage)
  })

})
