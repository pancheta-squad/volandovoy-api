class Healthz {
  static invoke () {
    return { status: 'Ok' }
  }
}

module.exports = Healthz
