const Healthz = require('./Healthz')

module.exports = {
  healthz: Healthz
}
