const Action = require('../actions/healthz')
const express = require('express')
const router = express.Router()
const SUCCESS = 200

router.get('/healthz', async (req, res, next) => {
  try {
    const response = await Action.healthz.invoke()
    res.status(SUCCESS).send(response)
  } catch(error) {
    next(error)
  }
})

module.exports = router
