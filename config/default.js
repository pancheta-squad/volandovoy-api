require('dotenv').config()

function die(error) {
  if (typeof error === 'string') {
    throw new Error(error)
  }
  throw error
}

const config = {
  environment:
    process.env.NODE_ENV ??
    die("Environment variable 'NODE_ENV' wasn't defined!"),
  version:
    process.env.API_VERSION ??
    die("Environment variable 'API_VERSION' wasn't defined!"),
  port:
    process.env.PORT ??
    die("Environment variable 'PORT' wasn't defined!"),
  logLevel:
    process.env.LOG_LEVEL ??
    die("Environment variable 'LOG_LEVEL' wasn't defined!")
}

module.exports = config
