const sleep = ms => {
  return new Promise((resolve, reject) => {
    let wait = setTimeout(() => {
      clearTimeout(wait)
      resolve()
    }, ms)
  })
}

module.exports = sleep
