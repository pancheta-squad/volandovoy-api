const error = async (err, req, res, next) => {
  const BAD_REQUEST = 400
  return res.status(BAD_REQUEST).send({ 
    message: err.message
  })
  next()
}

module.exports = { error }
