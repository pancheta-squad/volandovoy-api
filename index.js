const config = require('./config/default')
const express = require('express')
const app = express()
const apiVersion = `/api/${config.version}`
const healthzRoutes = require('./routes/healthz')
const middlewares = require('./middlewares')
const logger = require('./infrastructure/logger')
const sleep = require('./utils/sleep')

process.on('uncaughtException', async (error, source) => {
  logger.error(error.message, {stack: error.stack, type: source })
  await sleep(1000)
  process.exit(1)
});

process.on('unhandledRejection', async (error, source) => {
  logger.error(error, {stack: error.stack, type: 'unhandledRejection'})
  await sleep(1000)
  process.exit(1)
});

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(apiVersion, healthzRoutes)
app.use(middlewares.error)

module.exports = app
