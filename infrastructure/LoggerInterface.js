class LoggerInterface {
  
  fatal(message) {
    throw new Error('Logger fatal method must be implemented')
  }

  error(message) {
    throw new Error('Logger error method must be implemented')
  }

  warn(message) {
    throw new Error('Logger warn method must be implemented')
  }

  info(message) {
    throw new Error('Logger info method must be implemented')
  }
  
  debug(message) {
    throw new Error('Logger debug method must be implemented')
  }

}

module.exports = LoggerInterface
