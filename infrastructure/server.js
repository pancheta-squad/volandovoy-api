const config = require('../config/default')
const app = require('../index')
const http = require('http')
const RESTAPI_PORT = config.port

const onError = (error) => {
  console.error(error.message, { stack: error.stack })
}

const onListening = () => {
  const addr = server.address()
  console.info(`Server is listening on port ${addr.port}`)
}

app.set('port', RESTAPI_PORT)

const server = http.createServer(app)
server.listen(RESTAPI_PORT)
server.on('error', onError)
server.on('listening', onListening)
