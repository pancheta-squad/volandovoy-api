const pino = require('pino');
const config = require('../config/default')
const LoggerInterface = require('./LoggerInterface')

class PinoLogger extends LoggerInterface {
  
  constructor() {
    super()

    this.logger = pino({
      level: config.logLevel || 'info',
    },
    pino.multistream([
      { stream: process.stdout },
      { stream: pino.destination(`${__dirname}/../logs/${config.environment}.log`) }
    ])
    )
  }

  fatal(message, data = {}) {
    this.#childLogger(data).fatal(message)
  }

  error(message, data = {}) {
    this.#childLogger(data).error(message)
  }

  warn(message, data = {}) {
    this.#childLogger(data).warn(message)
  }

  info(message, data = {}) {
    this.#childLogger(data).info(message)
  }
  
  debug(message, data = {}) {
    this.#childLogger(data).debug(message)
  }

  #childLogger(data) {
    return this.logger.child(data)
  }

}

module.exports = new PinoLogger()

